﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7_task1
{
    class Program
    {
        public const int count = 19; // кіл-сть вершин
        public static string[] Cities =
        {
            "Київ",
            "Житомир",
            "Біла церква",
            "Прилуки",
            "Новоград-Волинський",
            "Бердичів",
            "Шепетівка",
            "Умань",
            "Черкаси",
            "Полтава",
            "Суми",
            "Миргород",
            "Рівне",
            "Вінниця",
            "Кременчук",
            "Харків",
            "Луцьк",
            "Хмельницький",
            "Тернопіль",
        };
        public static byte[,] AdjacencyMatrix = // матриця суміжності
        {
            {0, 135, 78, 128,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0, 80, 38, 115,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0, 115, 146, 181,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0, 175, 109,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0, 100,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0, 73,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0, 105,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0, 130,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0, 68,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0, 110,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0, 104},
            {0,   0,  0,   0,  0,  0,   0,   0,   0,   0,   0,   0,   0,  0,   0,   0,  0,   0,   0},
        };

        public static bool[] visited = new bool[count]; // масив пройдених вершин
        public static List<int> list = new List<int>(); // стек
        public static void DFS(int f, int sum = 0)
        {
            if (!visited[f])
            {
                visited[f] = true; // позначка, що вершину пройдено
                list.Add(f);
                for (int i = 0; i < count; i++) // прохід по вершинам
                {
                    if (AdjacencyMatrix[f, i] != 0) // не виконємо якщо відстань 0
                    {
                        sum += AdjacencyMatrix[f, i]; // додавання відстані до результату
                        DFS(i, sum);
                        Console.Write("path: ");
                        foreach (int city in list)
                        {
                            Console.Write($"{Cities[city]} -- ");
                        }
                        Console.Write($": {sum} km\n");
                        sum -= AdjacencyMatrix[f, i]; // віднімання відвіданої відстані
                        list.RemoveAt(list.Count - 1); // видалення відвіданої вершини
                    }
                }
            }
        }

        public static Queue<int> queue = new Queue<int>(); // черга
        public static List<int> distance = new List<int>(); // відстані між містами
        public static void BFS()
        {
            int n = 0;
            string start = Cities[n];
            visited[n] = true; // позначка, що вершину пройдено
            queue.Enqueue(n);
            distance.Add(0);
            while (queue.Count != 0) // додавання вершини до черги
            {
                n = queue.Dequeue(); // видаляємо перший елемент черги
                for (int i = 0; i < count; i++) // прохід по вершинам
                {
                    if (AdjacencyMatrix[n, i] != 0 && !visited[i])
                    {
                        visited[i] = true; // вершину пройдено
                        queue.Enqueue(i);
                        distance.Add(AdjacencyMatrix[n, i] + distance[n]); // додаємо дистанцію
                        Console.WriteLine($"distance: {start} -- {Cities[i]}: {distance[i]} km");
                    }
                }
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;
            for (int i = 0; i < count; i++)
            {
                visited[i] = false;
            }
            Console.WriteLine("BFS");
            BFS();
            for (int i = 0; i < count; i++)
            {
                visited[i] = false;
            }
            Console.WriteLine("\nDFS");
            DFS(0);
            Console.ReadKey();
        }
    }
}
